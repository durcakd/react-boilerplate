var webpack = require('webpack');
var Visualizer = require('webpack-visualizer-plugin');

module.exports = {
  entry: {
    'app': [
      //'webpack-dev-server/client?http://localhost:8080',
      //'webpack/hot/only-dev-server',
      'react-hot-loader/patch',
      './src/index.js'
    ]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        enforce: 'pre',
        exclude: /node_modules/,
        loader: 'eslint-loader'
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  output: {
    path: __dirname + '/dist',
    publicPath: '/',
    filename: 'bundle.js'
  },
  //devtool: "cheap-module-source-map",
  devServer: {
    inline: true,
    contentBase: './dist',
    hot: true,
    historyApiFallback: true
  },
  plugins: [
    new webpack.ProvidePlugin({
      'fetch': 'imports-loader?this=>global!exports-loader?global.fetch!whatwg-fetch'
    }),
    new Visualizer()
  ]
};
