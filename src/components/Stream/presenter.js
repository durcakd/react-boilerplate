import React, { Component } from 'react';
import { CLIENT_ID } from '../../constants/auth';


class Stream extends Component {

  componentDidUpdate() {
    const audioElement = this.audio;

    if (!audioElement) { return; }

    const { activeTrack } = this.props;

    if (activeTrack) {
      audioElement.play();
    } else {
      audioElement.pause();
    }
  }

  render() {
    const { user, tracks = [], activeTrack, onAuth, onPlay } = this.props;

    return (
      <div>
        <div>
          {
            user ?
              <div>{user.username}</div> :
              <button onClick={onAuth} type="button">Login</button>
          }
        </div>
        <br />
        <div>
          {
            tracks.map(track => (
              <div className="track" key={track.origin.id}>
                {track.origin.title}
                <button type="button" onClick={() => onPlay(track)}>Play</button>
              </div>
            ))
          }
        </div>
        {
          activeTrack ?
            <audio id="audio" ref={node => (this.audio = node)} src={`${activeTrack.origin.stream_url}?client_id=${CLIENT_ID}`} /> :
            null
        }
      </div>
    );
  }
}

Stream.propTypes = {
  user: React.PropTypes.shape({
    username: React.PropTypes.string.isRequired
  }).isRequired,
  tracks: React.PropTypes.arrayOf(
    React.PropTypes.shape({
      origin: React.PropTypes.shape({
        id: React.PropTypes.number.isRequired,
        title: React.PropTypes.string.isRequired
      })
    })
  ).isRequired,
  activeTrack: React.PropTypes.shape({
    origin: React.PropTypes.shape({
      stream_url: React.PropTypes.string.isRequired
    })
  }).isRequired,
  onAuth: React.PropTypes.func.isRequired,
  onPlay: React.PropTypes.func.isRequired
};

export default Stream;
